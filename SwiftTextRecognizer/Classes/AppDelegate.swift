import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate {

    var dependencyProvider: DependencyProvider
    var windowController: WindowController

    override init() {
        self.dependencyProvider = DefaultDependencyProvider()
        self.windowController = WindowController(dependencyProvider: dependencyProvider)
    }

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        dependencyProvider.setup(windowController: windowController)
        return true
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        windowController.makeAndPresentInitialViewController()
        return true
    }
}

