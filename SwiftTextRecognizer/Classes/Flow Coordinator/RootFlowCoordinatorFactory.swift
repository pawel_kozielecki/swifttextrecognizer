import Foundation
import UIKit

protocol RootFlowCoordinatorFactory {

    func makeNextRootFlowCoordinator() -> RootFlowCoordinator
}

class DefaultRootFlowCoordinatorFactory: RootFlowCoordinatorFactory {

    unowned let dependencyProvider: DependencyProvider

    init(dependencyProvider: DependencyProvider) {
        self.dependencyProvider = dependencyProvider
    }

    func makeNextRootFlowCoordinator() -> RootFlowCoordinator {
        //
        // Because we have only one flow, there is not much to choose from ;)
        // I know it's an overkill, but when we add eg. logged in / not logged in statuses,
        // ... app could easily navigate between the root flows, depending on initial app state ;)
        return TextRecognitionFlowCoordinator(dependencyProvider: dependencyProvider)
    }
}
