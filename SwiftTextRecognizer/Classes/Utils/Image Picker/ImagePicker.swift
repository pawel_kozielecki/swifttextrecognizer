import Foundation
import UIKit

protocol ImagePickerDelegate: class {
    func imagePicker(_ picker: ImagePicker, didFinishWithImage image: UIImage?)
    func imagePickerDidCancel(_ picker: ImagePicker)
}

class ImagePicker: NSObject {

    weak var presentingViewController: UIViewController?
    weak var delegate: ImagePickerDelegate?
    var backgroundQueueExecutor: AsynchronousOperationsExecutor = BackgroundQueueOperationsExecutor()
    var mainQueueExecutor: AsynchronousOperationsExecutor = MainQueueOperationsExecutor()

    init(presentingViewController: UIViewController) {
        self.presentingViewController = presentingViewController
    }

    func showImagePicker() {
        let alertController = UIAlertController(title: "IMAGE_PICKER_TITLE".localized, message: nil, preferredStyle: .actionSheet)
        let takePhotoAction = UIAlertAction(title: "IMAGE_PICKER_TAKE_PHOTO".localized, style: .default) { 
            [weak self] _ in
            self?.takePhoto()
        }
        let choosePhotoAction = UIAlertAction(title: "IMAGE_PICKER_CHOOSE_PHOTO".localized, style: .default) {
            [weak self] _ in
            self?.choosePhoto()
        }
        let cancelAction = UIAlertAction(title: "CANCEL".localized, style: .cancel)
        alertController.addAction(takePhotoAction)
        alertController.addAction(choosePhotoAction)
        alertController.addAction(cancelAction)

        presentingViewController?.present(alertController, animated: true)
    }
}

extension ImagePicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true) {
            [weak self] in

            self?.backgroundQueueExecutor.execute {
                let image = info[.originalImage] as? UIImage
                let correctedImage = image?.imageWithCorrectedOrientation()

                self?.mainQueueExecutor.execute {
                    [weak self] in
                    guard let strongSelf = self else {
                        return
                    }

                    strongSelf.delegate?.imagePicker(strongSelf, didFinishWithImage: correctedImage)
                }
            }
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            [weak self] in
            guard let strongSelf = self else {
                return
            }

            strongSelf.delegate?.imagePickerDidCancel(strongSelf)
        }
    }
}

private extension ImagePicker {
    func takePhoto() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = .camera
            presentingViewController?.present(imagePickerController, animated: true)
        }
    }

    func choosePhoto() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = .photoLibrary
            presentingViewController?.present(imagePickerController, animated: true)
        }
    }
}

extension ImagePickerDelegate {

    func imagePickerDidCancel(_ picker: ImagePicker) {
    }
}
