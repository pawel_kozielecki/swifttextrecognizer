import Foundation
import UIKit

protocol ThumbnailGenerator: class {
    func getThumbnail(forImage image: UIImage, size: CGSize, callback: ((UIImage?) -> Void)?)
}

class DefaultThumbnailGenerator: ThumbnailGenerator {

    var resizer = ImageResizer()

    func getThumbnail(forImage image: UIImage, size: CGSize, callback: ((UIImage?) -> Void)?) {
        let thumbnail = resizer.resize(image: image, to: size)
        callback?(thumbnail)
    }
}
