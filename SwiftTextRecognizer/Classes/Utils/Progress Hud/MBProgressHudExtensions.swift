import Foundation
import MBProgressHUD
import UIKit

extension MBProgressHUD {

    class func showPreloaderView(addedTo view: UIView, animated: Bool = true) -> MBProgressHUD {
        let preloaderHud = MBProgressHUD.showAdded(to: view, animated: animated)
        preloaderHud.mode = .indeterminate
        preloaderHud.bezelView.style = .solidColor
        preloaderHud.bezelView.color = .ocrWarmGrey
        preloaderHud.margin = 30
        preloaderHud.label.text = "RECOGNIZING_TEXT".localized
        preloaderHud.backgroundView.style = .blur
        return preloaderHud
    }
}
