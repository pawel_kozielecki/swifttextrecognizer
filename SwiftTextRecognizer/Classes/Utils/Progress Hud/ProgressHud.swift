import Foundation
import UIKit
import MBProgressHUD

protocol ProgressHud {
    func show()
    func hide()
}

class DefaultProgressHud: ProgressHud {

    weak var visibleViewControllerProvider: VisibleViewControllerProvider?
    weak var view: UIView?
    private (set) var currentHud: MBProgressHUD?

    init(visibleViewControllerProvider: VisibleViewControllerProvider) {
        self.visibleViewControllerProvider = visibleViewControllerProvider
    }

    init(view: UIView) {
        self.view = view
    }

    func show() {
        if let presentingView = getPresentingView(), currentHud == nil {
            currentHud = showHud(addedTo: presentingView, animated: true)
        }
    }

    func hide() {
        currentHud?.hide(animated: true)
        currentHud = nil
    }
}

extension DefaultProgressHud {

    func getPresentingView() -> UIView? {
        if let view = view {
            return view
        }
        return visibleViewControllerProvider?.visibleView()
    }

    func showHud(addedTo view: UIView, animated: Bool) -> MBProgressHUD {
        return MBProgressHUD.showPreloaderView(addedTo: view, animated: animated)
    }
}
