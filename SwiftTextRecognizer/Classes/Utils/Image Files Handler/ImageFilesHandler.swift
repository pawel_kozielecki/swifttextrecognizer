import Foundation
import UIKit

protocol ImageFilesHandler: class {

    func readImage(id: String, isThumbnail: Bool) -> UIImage?
    @discardableResult func saveImage(_ image: UIImage, asThumbnail: Bool) -> String?
    @discardableResult func deleteImage(withId id: String, isThumbnail: Bool) -> Bool

    func readImage(id: String, isThumbnail: Bool, callback: ((UIImage?) -> Void)?)
    func saveImage(_ image: UIImage, asThumbnail: Bool, callback: ((String?) -> Void)?)
    func deleteImage(withId id: String, isThumbnail: Bool, callback: ((Bool) -> Void)?)
}

class DefaultImageFilesHandler: ImageFilesHandler {

    let fileExtension = ".jpg"
    let imagesPath = "images"
    let thumbnailsPath = "thumbnails"
    var fileManager: FileManager = FileManager()
    var backgroundQueueExecutor: AsynchronousOperationsExecutor = BackgroundQueueOperationsExecutor()
    var mainQueueExecutor: AsynchronousOperationsExecutor = MainQueueOperationsExecutor()

    func readImage(id: String, isThumbnail: Bool) -> UIImage? {
        let filePath = getFilePath(isThumbnail: isThumbnail, id: id)
        let image = UIImage(contentsOfFile: filePath.relativePath)
        return image
    }

    func readImage(id: String, isThumbnail: Bool, callback: ((UIImage?) -> Void)?) {
        backgroundQueueExecutor.execute {
            [weak self] in
            let image = self?.readImage(id: id, isThumbnail: isThumbnail)
            self?.mainQueueExecutor.execute {
                callback?(image)
            }
        }
    }

    @discardableResult func saveImage(_ image: UIImage, asThumbnail: Bool) -> String? {
        fileManager.ensureDirectoryExists(atURL: getImagesFolderPath(isThumbnail: asThumbnail))
        let imageId = UUID().uuidString
        let path = getFilePath(isThumbnail: asThumbnail, id: imageId)
        if let data = image.jpegData(compressionQuality: AppConstants.imagesCompression) {
            do {
                try data.write(to: path, options: [.atomic])
                return imageId
            } catch {
                return nil
            }
        } else {
            log(kind: .warning, message: "DefaultImageFilesHandler:saveImage:failed - could not save image \(image) to path: \(path.absoluteString)")
            return nil
        }
    }

    func saveImage(_ image: UIImage, asThumbnail: Bool, callback: ((String?) -> Void)?) {
        backgroundQueueExecutor.execute {
            [weak self] in
            let imageId = self?.saveImage(image, asThumbnail: asThumbnail)
            self?.mainQueueExecutor.execute {
                callback?(imageId)
            }
        }
    }

    @discardableResult func deleteImage(withId id: String, isThumbnail: Bool) -> Bool {
        let path = getFilePath(isThumbnail: isThumbnail, id: id)
        if (fileManager.fileExists(atPath: path.relativePath)) {
            do {
                try fileManager.removeItem(atPath: path.relativePath)
                return true
            } catch {
                log(message: "DefaultImageFilesHandler:deleteImage:failed - could not delete file at path: \(path.relativePath)")
                return false
            }
        }
        return true
    }

    func deleteImage(withId id: String, isThumbnail: Bool, callback: ((Bool) -> Void)?) {
        backgroundQueueExecutor.execute {
            [weak self] in
            let success = self?.deleteImage(withId: id, isThumbnail: isThumbnail)
            self?.mainQueueExecutor.execute {
                callback?(success == true)
            }
        }
    }
}

private extension DefaultImageFilesHandler {

    func getFilePath(isThumbnail: Bool, id: String) -> URL {
        let folderPath = getImagesFolderPath(isThumbnail: isThumbnail)
        let filePath = folderPath.appendingPathComponent(id + fileExtension)
        return filePath
    }

    private func getImagesFolderPath(isThumbnail: Bool) -> URL {
        let base = fileManager.applicationSupportDirectoryURL
        let folderPath = base.appendingPathComponent(isThumbnail ? thumbnailsPath : imagesPath)
        return folderPath
    }
}
