import Foundation
import UIKit

extension UIImage {

    func resizedImage(newSize: CGSize) -> UIImage {
        guard self.size != newSize else {
            return self
        }

        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0);
        draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))

        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else {
            return self
        }
        UIGraphicsEndImageContext()

        return newImage
    }

/*
 *  This is a helper method transforming images obtained from UIImagePickerController, who have improper orientation
 *  ... the method analyzes image orientation and transforms it to be displayed as in image gallery or on camera preview
 *  ... original source: https://gist.github.com/schickling/b5d86cb070130f80bb40
 */
    func imageWithCorrectedOrientation() -> UIImage {
        guard imageOrientation != UIImage.Orientation.up else {
            return self
        }

        guard let cgImage = self.cgImage else {
            return self
        }

        guard let colorSpace = cgImage.colorSpace,
              let cgContext = CGContext(data: nil,
                      width: Int(size.width), height: Int(size.height),
                      bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0,
                      space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            return self
        }

        var transform: CGAffineTransform = CGAffineTransform.identity
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            break
        default:
            break
        }

        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        default:
            break
        }

        cgContext.concatenate(transform)

        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            cgContext.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            cgContext.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }

        guard let newCGImage = cgContext.makeImage() else {
            return self
        }

        return UIImage(cgImage: newCGImage, scale: 1, orientation: .up)
    }
}
