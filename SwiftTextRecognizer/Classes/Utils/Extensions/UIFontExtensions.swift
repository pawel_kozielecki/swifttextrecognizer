import Foundation
import UIKit

extension UIFont {

    class var ocrHeaderFont: UIFont {
        return UIFont.systemFont(ofSize: 20.0, weight: .medium)
    }

    class var ocrTextFont: UIFont {
        return UIFont.systemFont(ofSize: 15.0, weight: .regular)
    }

    class var ocrCellTextFont: UIFont {
        return UIFont.systemFont(ofSize: 17.0, weight: .regular)
    }
}
