import Foundation
import UIKit

extension UIImageView {

    class func makeGenericImageView(image: UIImage = UIImage()) -> UIImageView {
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }
}
