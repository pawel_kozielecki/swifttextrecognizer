import Foundation

extension String {

    var localized: String {
        return NSLocalizedString(self, comment: "")
    }

    func ellipsis(length: Int, trailing: String = "…") -> String {
        return (self.count > length) ? self.prefix(length) + trailing : self
    }
}
