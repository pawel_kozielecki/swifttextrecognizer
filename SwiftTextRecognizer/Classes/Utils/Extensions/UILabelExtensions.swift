import Foundation
import UIKit

extension UILabel {

    class func makeHeaderLabel(title: String, alignment: NSTextAlignment = .center) -> UILabel {
        let label = UILabel()
        label.font = .ocrHeaderFont
        label.numberOfLines = 0
        label.text = title
        label.textAlignment = alignment
        return label
    }

    class func makeNoItemsLabel(title: String, alignment: NSTextAlignment = .center) -> UILabel {
        let label = UILabel()
        label.text = title
        label.font = .ocrTextFont
        label.numberOfLines = 0
        label.textAlignment = alignment
        return label
    }
}
