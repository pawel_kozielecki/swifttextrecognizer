import Foundation
import UIKit

extension UIButton {

    class func makeGenericButton(title: String) -> UIButton {
        let button = UIButton(type: .system)
        button.setTitle(title, for: .normal)
        button.contentMode = .center
        button.backgroundColor = .white
        button.layer.cornerRadius = 5
        return button
    }

    static func makeGenericBackButton() -> UIButton {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "icon-back.png"), for: .normal)
        return button
    }
}
