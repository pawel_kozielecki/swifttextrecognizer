import Foundation

extension FileManager {

    var applicationSupportDirectoryURL: URL {
        return try! self.url(for: .applicationSupportDirectory,
                in: .userDomainMask,
                appropriateFor: nil,
                create: true)
    }

    func ensureDirectoryExists(atURL directoryURL: URL) {
        if !fileExists(atPath: directoryURL.path) {
            try! createDirectory(at: directoryURL, withIntermediateDirectories: true)
        }
    }
}
