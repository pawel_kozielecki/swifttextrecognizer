import Foundation
import UIKit

extension UITextView {

    class func makeGenericTextView(text: String, isEditable: Bool = false) -> UITextView {
        let textView = UITextView()
        textView.text = text
        textView.isEditable = isEditable
        return textView
    }
}
