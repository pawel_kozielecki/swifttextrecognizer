import Foundation
import Toucan
import UIKit

enum ImageResizerWorkMode {
    case fitIn
    case keepRatio
}

class ImageResizer {

    func resize(image: UIImage, to size: CGSize, workMode: ImageResizerWorkMode = .fitIn) -> UIImage? {
        switch workMode {
        case .fitIn:
            return Toucan(image: image).resize(size, fitMode: Toucan.Resize.FitMode.crop).image
        case .keepRatio:
            return Toucan(image: image).resize(size, fitMode: Toucan.Resize.FitMode.clip).image
        }
    }
}
