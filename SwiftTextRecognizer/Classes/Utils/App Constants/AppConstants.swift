import Foundation
import UIKit

struct AppConstants {

    static let genericButtonHeight = CGFloat(50)
    static let backArrowButtonSize = CGSize(width: 50, height: 50)
    static let genericButtonSpacing = CGFloat(20)
    static let genericItemsSpacing = CGFloat(10)
    static let genericHeaderSpacing = CGFloat(30)
    static let genericTextViewHeight = CGFloat(200)
    static let enhancedTextViewHeight = CGFloat(300)
    static let imagesCompression = CGFloat(0.5)
    static let thumbnailSize = CGSize(width: 50, height: 50)
    static let largeTableViewRowHeight = CGFloat(100)
    static let ellipsisLength = 100
}
