import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class WindowControllerSpec: QuickSpec {
    override func spec() {
        describe("WindowController") {

            var fakeDependencyProvider: FakeDependencyProvider!
            var sut: WindowController?

            beforeEach {
                fakeDependencyProvider = FakeDependencyProvider()
                sut = WindowController(dependencyProvider: fakeDependencyProvider)
            }

            it("should use provided dependency provider") {
                expect(sut?.dependencyProvider) === fakeDependencyProvider
            }

            it("should create root view controller") {
                expect(sut?.visibleViewController()) === sut?.rootViewController
            }

            describe("setting up Window and Root Controller") {

                beforeEach {
                    sut?.makeAndPresentInitialViewController()
                }

                it("should make root view controller visible") {
                    expect(sut?.window.rootViewController) === sut?.rootViewController
                }
            }
        }
    }
}
