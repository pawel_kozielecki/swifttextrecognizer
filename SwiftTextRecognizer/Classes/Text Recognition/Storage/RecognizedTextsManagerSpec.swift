import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class DefaultRecognizedTextsManagerSpec: QuickSpec {
    override func spec() {
        describe("DefaultRecognizedTextsManager") {

            let fixtureKey = "recognizedTexts"

            var fakeStorage: FakeLocalStorage!
            var sut: DefaultRecognizedTextsManager?

            beforeEach {
                fakeStorage = FakeLocalStorage()
                sut = DefaultRecognizedTextsManager()
                sut?.storage = fakeStorage
            }

            describe("initial value") {
                var recognizedTexts: [RecognizedTextModel]!

                beforeEach {
                    recognizedTexts = sut!.getRecognizedTexts()
                }

                it("should return empty recognized texts") {
                    expect(recognizedTexts).to(beEmpty())
                }
            }

            describe("managing recognized texts") {
                var fixtureRecognizedTexts: [RecognizedTextModel]!
                var fixtureRecognizedTextsData: Data!

                beforeEach {
                    fixtureRecognizedTexts = [RecognizedTextModel(thumbnailId: "fixtureThId", imageId: "fixtureImgId", recognizedText: "fixtureText")]
                    fixtureRecognizedTextsData = try! JSONEncoder().encode(fixtureRecognizedTexts)
                }

                describe("writing") {
                    beforeEach {
                        sut?.store(recognizedTexts: fixtureRecognizedTexts)
                    }

                    it("should store provided texts") {
                        fakeStorage.verifyCall(withIdentifier: "write", arguments: [fixtureKey, fixtureRecognizedTextsData])
                    }

                    describe("removing") {
                        var recognizedTexts: [RecognizedTextModel]!

                        beforeEach {
                            sut?.remove(recognizedText: fixtureRecognizedTexts.first!)
                            recognizedTexts = sut!.getRecognizedTexts()
                        }

                        it("should return empty recognized texts") {
                            expect(recognizedTexts).to(beEmpty())
                        }
                    }
                }

                describe("reading") {
                    beforeEach {
                        fakeStorage.simulatedValue = fixtureRecognizedTextsData
                    }

                    it("should return recognized texts array") {
                        expect(sut?.getRecognizedTexts()) == fixtureRecognizedTexts
                    }
                }

                describe("invalidating") {
                    beforeEach {
                        sut?.invalidateRecognizedTexts()
                    }

                    it("should invalidate drone manufacturers") {
                        fakeStorage.verifyCall(withIdentifier: "invalidate", arguments: [fixtureKey])
                    }
                }
            }
        }
    }
}
