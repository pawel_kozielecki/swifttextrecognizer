import Foundation

protocol RecognizedTextsProvider: class {
    func getRecognizedTexts() -> [RecognizedTextModel]
}

protocol RecognizedTextsStorage: class {
    func store(recognizedTexts: [RecognizedTextModel])
    func remove(recognizedText: RecognizedTextModel)
    func invalidateRecognizedTexts()
}

protocol RecognizedTextsManager: RecognizedTextsProvider, RecognizedTextsStorage {
}

class DefaultRecognizedTextsManager: RecognizedTextsManager {
    let key = "recognizedTexts"

    var storage: LocalStorage = UserDefaultsStorage()

    func getRecognizedTexts() -> [RecognizedTextModel] {
        guard let data = storage.readData(key: key) else {
            return []
        }

        guard let recognizedTexts = try? JSONDecoder().decode([RecognizedTextModel].self, from: data) else {
            return []
        }

        return recognizedTexts
    }

    func store(recognizedTexts: [RecognizedTextModel]) {
        if let data = try? JSONEncoder().encode(recognizedTexts) {
            storage.write(key: key, value: data)
        } else {
            invalidateRecognizedTexts()
        }
    }

    func remove(recognizedText: RecognizedTextModel) {
        var texts = getRecognizedTexts()
        if let index = texts.firstIndex(of: recognizedText) {
            texts.remove(at: index)
            store(recognizedTexts: texts)
        }
    }

    func invalidateRecognizedTexts() {
        storage.invalidate(key: key)
    }
}
