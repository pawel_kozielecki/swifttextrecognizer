import Foundation
import UIKit

class TextRecognitionFlowCoordinator: RootFlowCoordinator {
    unowned let dependencyProvider: DependencyProvider

    weak var rootFlowCoordinatorDelegate: RootFlowCoordinatorDelegate?
    var navigationController: UINavigationController

    var rootViewController: UIViewController {
        return navigationController
    }

    init(dependencyProvider: DependencyProvider) {
        self.dependencyProvider = dependencyProvider
        self.navigationController = TextRecognitionFlowCoordinator.makeNavigationController()
    }

    func start() {
        let initialViewController = RecognizedTextsViewController(
                recognizedTextsManager: dependencyProvider.recognizedTextsManager,
                imageFilesHandler: dependencyProvider.imageFilesHandler)
        initialViewController.delegate = self
        navigationController.pushViewController(initialViewController, animated: false)
    }
}

extension TextRecognitionFlowCoordinator: RecognizedTextsViewControllerDelegate {

    func recognizedTextsViewController(_ controller: RecognizedTextsViewController, didAddNewImage image: UIImage) {
        let photoSelectionViewController = TextRecognitionViewController(selectedImage: image, textRecognizer: dependencyProvider.textRecognizer)
        photoSelectionViewController.delegate = self
        navigationController.pushViewController(photoSelectionViewController, animated: true)
    }

    func recognizedTextsViewController(_ controller: RecognizedTextsViewController, didRequestShowingImage image: UIImage, recognizedText: String) {
        let resultViewController = TextRecognitionResultViewController(
                toDisplaySavedImage: image,
                withRecognizedText: recognizedText,
                imageFilesHandler: dependencyProvider.imageFilesHandler)
        resultViewController.delegate = self
        navigationController.pushViewController(resultViewController, animated: true)
    }
}

extension TextRecognitionFlowCoordinator: TextRecognitionViewControllerDelegate {

    func textRecognitionViewController(_ viewController: TextRecognitionViewController, didRecognizeText text: String, forPhoto photo: UIImage) {
        let resultViewController = TextRecognitionResultViewController(
                toDisplayPreviewOfImage: photo,
                withRecognizedText: text,
                imageFilesHandler: dependencyProvider.imageFilesHandler,
                recognizedTextsManager: dependencyProvider.recognizedTextsManager)
        resultViewController.delegate = self
        navigationController.pushViewController(resultViewController, animated: true)
    }

    func textRecognitionViewControllerDidCancel(_ viewController: TextRecognitionViewController) {
        navigationController.popViewController(animated: true)
    }
}

extension TextRecognitionFlowCoordinator: TextRecognitionResultViewControllerDelegate {

    func textRecognitionResultViewControllerDidFinish(_ viewController: TextRecognitionResultViewController) {
        navigationController.popToRootViewController(animated: true)
    }
}

private extension TextRecognitionFlowCoordinator {

    class func makeNavigationController() -> UINavigationController {
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = false
        // todo: add some bells and whistles to nav bar
        return navigationController
    }
}

