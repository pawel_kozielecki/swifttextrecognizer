import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class TextRecognitionFlowCoordinatorSpec: QuickSpec {
    override func spec() {
        describe("TextRecognitionFlowCoordinator") {

            var fakeDependencyProvider: FakeDependencyProvider!
            var sut: TextRecognitionFlowCoordinator?

            beforeEach {
                fakeDependencyProvider = FakeDependencyProvider()
                sut = TextRecognitionFlowCoordinator(dependencyProvider: fakeDependencyProvider)
            }

            it("should use provided dependency provider") {
                expect(sut?.dependencyProvider) === fakeDependencyProvider
            }

            it("should create root navigation controller") {
                expect(sut?.rootViewController as? UINavigationController).notTo(beNil())
            }

            describe("initial controller") {
                var fakeNavigationController: FakeUINavigationController!
                var recognizedTextsViewController: RecognizedTextsViewController?

                beforeEach {
                    fakeNavigationController = FakeUINavigationController()
                    sut?.navigationController = fakeNavigationController
                    sut?.start()
                    recognizedTextsViewController = fakeNavigationController.lastPushedViewController as? RecognizedTextsViewController
                }

                it("should create proper initial view controller and become its delegate") {
                    expect(recognizedTextsViewController?.delegate) === sut
                }

                describe("adding new image") {
                    var textRecognitionViewController: TextRecognitionViewController?

                    beforeEach {
                        sut?.recognizedTextsViewController(recognizedTextsViewController!, didAddNewImage: UIImage())
                        textRecognitionViewController = fakeNavigationController.lastPushedViewController as? TextRecognitionViewController
                    }

                    it("should create and show TextRecognitionViewController and become its delegate") {
                        expect(textRecognitionViewController?.delegate) === sut
                    }

                    describe("successful text recognition") {
                        var textRecognitionResultViewController: TextRecognitionResultViewController?

                        beforeEach {
                            sut?.textRecognitionViewController(textRecognitionViewController!, didRecognizeText: "", forPhoto: UIImage())
                            textRecognitionResultViewController = fakeNavigationController.lastPushedViewController as? TextRecognitionResultViewController
                        }

                        it("should create and show TextRecognitionResultViewController and become its delegate") {
                            expect(textRecognitionResultViewController?.delegate) === sut
                        }

                        describe("going back or saving recognized text") {
                            beforeEach {
                                sut?.textRecognitionResultViewControllerDidFinish(textRecognitionResultViewController!)
                            }

                            it("should return to recognized texts screen") {
                                fakeNavigationController.verifyCall(withIdentifier: "popToRootViewController", arguments: [true])
                            }
                        }
                    }

                    describe("rejecting image") {
                        beforeEach {
                            sut?.textRecognitionViewControllerDidCancel(textRecognitionViewController!)
                        }

                        it("should return to recognized texts screen") {
                            fakeNavigationController.verifyCall(withIdentifier: "popViewController", arguments: [true])
                        }
                    }
                }

                describe("selecting recognized text for preview") {
                    var textRecognitionResultViewController: TextRecognitionResultViewController?

                    beforeEach {
                        sut?.recognizedTextsViewController(recognizedTextsViewController!, didRequestShowingImage: UIImage(), recognizedText: "")
                        textRecognitionResultViewController = fakeNavigationController.lastPushedViewController as? TextRecognitionResultViewController
                    }

                    it("should create and show TextRecognitionResultViewController and become its delegate") {
                        expect(textRecognitionResultViewController?.delegate) === sut
                    }

                    describe("going back") {
                        beforeEach {
                            sut?.textRecognitionResultViewControllerDidFinish(textRecognitionResultViewController!)
                        }

                        it("should return to recognized texts screen") {
                            fakeNavigationController.verifyCall(withIdentifier: "popToRootViewController", arguments: [true])
                        }
                    }
                }
            }
        }
    }
}
