import Foundation
import UIKit
import TesseractOCR

protocol TextRecognizer: class {
    func recognizeText(forImage image: UIImage, callback: ((String?) -> Void)?)
}

protocol OCR: class {
    var image: UIImage { get set }
    var recognizedText: String? { get }
    func recognize() -> Bool
}

class DefaultTextRecognizer: TextRecognizer {

    var ocr: OCR? = G8Tesseract(language: "eng")
    var backgroundQueueExecutor: AsynchronousOperationsExecutor = BackgroundQueueOperationsExecutor()
    var mainQueueExecutor: AsynchronousOperationsExecutor = MainQueueOperationsExecutor()

    func recognizeText(forImage image: UIImage, callback: ((String?) -> Void)?) {
        log(message: "TextRecognizer:recognizeText:started")
        backgroundQueueExecutor.execute {
            [weak self] in
            self?.ocr?.image = image
            _ = self?.ocr?.recognize()
            self?.mainQueueExecutor.execute {
                [weak self] in
                let recognizedText = self?.ocr?.recognizedText
                log(message: "TextRecognizer:recognizeText:finished - recognized text: \(recognizedText ?? "")")
                callback?(recognizedText)
            }
        }
    }
}

extension G8Tesseract: OCR {
}
