import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class TextRecognizerSpec: QuickSpec {
    override func spec() {
        describe("TextRecognizer") {

            var fakeOCR: FakeOCR!
            var sut: DefaultTextRecognizer?

            beforeEach {
                fakeOCR = FakeOCR()
                sut = DefaultTextRecognizer()
                sut?.ocr = fakeOCR
                sut?.backgroundQueueExecutor = FakeAsynchronousOperationsExecutor()
                sut?.mainQueueExecutor = FakeAsynchronousOperationsExecutor()
            }

            describe("recognizing text") {
                var recognizedText: String?
                var fixtureImage: UIImage!

                beforeEach {
                    fixtureImage = UIImage(named: "icon-back")!
                }

                describe("success") {
                    let fixtureText = "fixtureText"

                    beforeEach {
                        fakeOCR.simulatedRecognizedText = fixtureText
                        sut?.recognizeText(forImage: fixtureImage) {
                            text in
                            recognizedText = text
                        }
                    }

                    it("should pass image to ocr") {
                        expect(fakeOCR.image) === fixtureImage
                    }

                    it("should start recognizing text") {
                        fakeOCR.verifyCall(withIdentifier: "recognize")
                    }

                    it("should return recognized text") {
                        expect(recognizedText) == fixtureText
                    }
                }

                describe("failure") {
                    beforeEach {
                        sut?.recognizeText(forImage: fixtureImage) {
                            text in
                            recognizedText = text
                        }
                    }

                    it("should pass image to ocr") {
                        expect(fakeOCR.image) === fixtureImage
                    }

                    it("should start recognizing text") {
                        fakeOCR.verifyCall(withIdentifier: "recognize")
                    }

                    it("should NOT return recognized text") {
                        expect(recognizedText).to(beNil())
                    }
                }
            }
        }
    }
}

class FakeOCR: OCR, Mock {
    var storage: [RecordedCall] = []

    var image: UIImage = UIImage()
    var simulatedRecognizedText: String?

    var recognizedText: String? {
        return simulatedRecognizedText
    }

    func recognize() -> Bool {
        recordCall(withIdentifier: "recognize")
        return true
    }

//    func simulateRecognitionFinished(recognizedText: String?) {
//
//    }
}
