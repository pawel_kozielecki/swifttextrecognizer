import Foundation
import UIKit
import SnapKit

protocol TextRecognitionViewDelegate: class {
    func textRecognitionViewDidSelectPhoto(_ view: TextRecognitionView)
    func TextRecognitionViewDidCancel(_ view: TextRecognitionView)
}

class TextRecognitionView: UIView {

    let cancelButton = UIButton.makeGenericButton(title: "TEXT_RECOGNITION_VIEW_CANCEL".localized)
    let acceptButton = UIButton.makeGenericButton(title: "TEXT_RECOGNITION_VIEW_ACCEPT".localized)
    let header = UILabel.makeHeaderLabel(title: "TEXT_RECOGNITION_VIEW_HEADER".localized)
    let imageView: UIImageView

    weak var delegate: TextRecognitionViewDelegate?

    init(image: UIImage) {
        imageView = UIImageView.makeGenericImageView(image: image)

        super.init(frame: .zero)

        backgroundColor = .ocrWarmGrey
        arrangeSubviews()
        applyConstraints()

        acceptButton.addTarget(self, action: #selector(TextRecognitionView.didTapAcceptButton(_:)), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(TextRecognitionView.didTapCancelButton(_:)), for: .touchUpInside)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("not implemented")
    }
}

private extension TextRecognitionView {

    @objc func didTapAcceptButton(_ sender: UIButton) {
        delegate?.textRecognitionViewDidSelectPhoto(self)
    }

    @objc func didTapCancelButton(_ sender: UIButton) {
        delegate?.TextRecognitionViewDidCancel(self)
    }

    func arrangeSubviews() {
        addSubview(imageView)
        addSubview(header)
        addSubview(acceptButton)
        addSubview(cancelButton)
    }

    func applyConstraints() {
        header.snp.makeConstraints {
            make in
            make.top.equalTo(safeAreaLayoutGuide.snp.topMargin).inset(AppConstants.genericHeaderSpacing)
            make.leading.equalTo(safeAreaLayoutGuide.snp.leadingMargin).inset(AppConstants.genericHeaderSpacing)
            make.trailing.equalTo(safeAreaLayoutGuide.snp.trailingMargin).inset(AppConstants.genericHeaderSpacing)
        }
        cancelButton.snp.makeConstraints {
            make in
            make.height.equalTo(AppConstants.genericButtonHeight)
            make.width.equalToSuperview().dividedBy(2).inset(AppConstants.genericButtonSpacing)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottomMargin).inset(AppConstants.genericButtonSpacing)
            make.leading.equalTo(safeAreaLayoutGuide.snp.leadingMargin).offset(AppConstants.genericButtonSpacing)
        }
        acceptButton.snp.makeConstraints {
            make in
            make.height.equalTo(AppConstants.genericButtonHeight)
            make.width.equalToSuperview().dividedBy(2).inset(AppConstants.genericButtonSpacing)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottomMargin).inset(AppConstants.genericButtonSpacing)
            make.trailing.equalTo(safeAreaLayoutGuide.snp.trailingMargin).inset(AppConstants.genericButtonSpacing)
        }
        imageView.snp.makeConstraints {
            make in
            let verticalMargin = 2 * AppConstants.genericButtonSpacing + AppConstants.genericButtonHeight
            make.top.equalTo(safeAreaLayoutGuide.snp.topMargin).inset(verticalMargin)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottomMargin).inset(verticalMargin)
            make.leading.equalTo(safeAreaLayoutGuide.snp.leadingMargin).inset(AppConstants.genericItemsSpacing)
            make.trailing.equalTo(safeAreaLayoutGuide.snp.trailingMargin).inset(AppConstants.genericItemsSpacing)
        }
    }
}
