import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class PhotoSelectionViewControllerSpec: QuickSpec {
    override func spec() {
        describe("PhotoSelectionViewController") {

            let fixtureImage = UIImage()
            var fakeTextRecognizer: FakeTextRecognizer!
            var fakeDelegate: FakeTextRecognitionViewControllerDelegate!
            var fakeAlert: FakeInfoAlert!
            var sut: TextRecognitionViewController?

            beforeEach {
                fakeTextRecognizer = FakeTextRecognizer()
                fakeDelegate = FakeTextRecognitionViewControllerDelegate()
                fakeAlert = FakeInfoAlert()

                sut = TextRecognitionViewController(selectedImage: fixtureImage, textRecognizer: fakeTextRecognizer)

                sut?.delegate = fakeDelegate
                sut?.infoAlert = fakeAlert
                sut?.loadViewIfNeeded()
            }

            it("should use provided image") {
                expect(sut?.image) === fixtureImage
            }

            it("should use provided OCR") {
                expect(sut?.textRecognizer) === fakeTextRecognizer
            }

            describe("interaction with view") {
                var view: TextRecognitionView?

                beforeEach {
                    view = sut?.view as? TextRecognitionView
                }

                it("should be view delegate") {
                    expect(view?.delegate) === sut
                }

                describe("image rejected") {
                    beforeEach {
                        view?.cancelButton.simulateTap()
                    }

                    it("should notify delegate") {
                        fakeDelegate.verifyCall(withIdentifier: "cancel")
                    }
                }

                describe("image accepted") {
                    beforeEach {
                        view?.acceptButton.simulateTap()
                    }

                    it("should start recognition") {
                        fakeTextRecognizer.verifyCall(withIdentifier: "recognizeText", arguments: [fixtureImage.size])
                    }

                    describe("success") {
                        let fixtureRecognizedText = "fixtureRecognizedText"

                        beforeEach {
                            fakeTextRecognizer.simulateSuccess(text: fixtureRecognizedText)
                        }

                        it("should notify delegate") {
                            fakeDelegate.verifyCall(withIdentifier: "didRecognize", arguments: [fixtureRecognizedText, fixtureImage.size])
                        }
                    }

                    describe("failure") {
                        beforeEach {
                            fakeTextRecognizer.simulateFailure()
                        }

                        it("should show error alert") {
                            let title = "TEXT_RECOGNITION_VIEW_TEXT_RECOGNITION_ERROR_TITLE".localized
                            let message = "TEXT_RECOGNITION_VIEW_TEXT_RECOGNITION_ERROR_MESSAGE".localized
                            fakeAlert.verifyCall(withIdentifier: "show", arguments: [title, message])
                        }
                    }
                }
            }

            //  todo: add snapshot tests


        }
    }
}

class FakeTextRecognitionViewControllerDelegate: TextRecognitionViewControllerDelegate, Mock {
    var storage: [RecordedCall] = []

    func textRecognitionViewController(_ viewController: TextRecognitionViewController, didRecognizeText text: String, forPhoto photo: UIImage) {
        recordCall(withIdentifier: "didRecognize", arguments: [text, photo.size])
    }

    func textRecognitionViewControllerDidCancel(_ viewController: TextRecognitionViewController) {
        recordCall(withIdentifier: "cancel")
    }
}
