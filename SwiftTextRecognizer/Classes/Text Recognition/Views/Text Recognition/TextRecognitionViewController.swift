import Foundation
import UIKit

protocol TextRecognitionViewControllerDelegate: class {

    func textRecognitionViewController(_ viewController: TextRecognitionViewController, didRecognizeText text: String, forPhoto photo: UIImage)
    func textRecognitionViewControllerDidCancel(_ viewController: TextRecognitionViewController)
}

class TextRecognitionViewController: UIViewController {

    let image: UIImage
    let textRecognizer: TextRecognizer
    var progressHud: ProgressHud?
    var infoAlert: InfoAlert?
    weak var delegate: TextRecognitionViewControllerDelegate?

    init(selectedImage image: UIImage, textRecognizer: TextRecognizer) {
        self.image = image
        self.textRecognizer = textRecognizer

        super.init(nibName: nil, bundle: nil)

        progressHud = DefaultProgressHud(view: view)
        infoAlert = SimpleInfoAlert(presentingViewController: self)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let view = TextRecognitionView(image: image)
        view.delegate = self
        self.view = view
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

extension TextRecognitionViewController: TextRecognitionViewDelegate {
    func textRecognitionViewDidSelectPhoto(_ view: TextRecognitionView) {
        progressHud?.show()
        textRecognizer.recognizeText(forImage: image) {
            [weak self] text in
            guard let strongSelf = self else {
                return
            }

            if let text = text {
                log(message: "TextRecognitionViewController:textRecognition:success - text: \(text)")
                strongSelf.delegate?.textRecognitionViewController(strongSelf, didRecognizeText: text, forPhoto: strongSelf.image)
            } else {
                log(kind: .warning, message: "TextRecognitionViewController:textRecognition:failed")
                strongSelf.showErrorAlert()
            }
            self?.progressHud?.hide()
        }
    }

    func TextRecognitionViewDidCancel(_ view: TextRecognitionView) {
        delegate?.textRecognitionViewControllerDidCancel(self)
    }
}

private extension TextRecognitionViewController {

    func showErrorAlert() {
        let title = "TEXT_RECOGNITION_VIEW_TEXT_RECOGNITION_ERROR_TITLE".localized
        let message = "TEXT_RECOGNITION_VIEW_TEXT_RECOGNITION_ERROR_MESSAGE".localized
        infoAlert?.show(title: title, message: message, callback: nil)
    }
}
