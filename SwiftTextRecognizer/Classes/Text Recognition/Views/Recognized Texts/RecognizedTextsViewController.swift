import Foundation
import UIKit
import SnapKit

protocol RecognizedTextsViewControllerDelegate: class {

    func recognizedTextsViewController(_ controller: RecognizedTextsViewController, didAddNewImage image: UIImage)
    func recognizedTextsViewController(_ controller: RecognizedTextsViewController, didRequestShowingImage image: UIImage, recognizedText: String)
}

class RecognizedTextsViewController: UITableViewController {

    let recognizedTextsManager: RecognizedTextsManager
    let imageFilesHandler: ImageFilesHandler
    let noItemsLabel = UILabel.makeNoItemsLabel(title: "RECOGNIZED_TEXTS_SCREEN_NO_ITEMS".localized)

    var backgroundQueueExecutor: AsynchronousOperationsExecutor = BackgroundQueueOperationsExecutor()
    var mainQueueExecutor: AsynchronousOperationsExecutor = MainQueueOperationsExecutor()
    var alert: AcceptanceAlert?
    var imagePicker: ImagePicker?

    var tableViewModel = [RecognizedTextTableViewCellModel]()
    weak var delegate: RecognizedTextsViewControllerDelegate?

    init(recognizedTextsManager: RecognizedTextsManager, imageFilesHandler: ImageFilesHandler) {
        self.recognizedTextsManager = recognizedTextsManager
        self.imageFilesHandler = imageFilesHandler

        super.init(nibName: nil, bundle: nil)

        alert = SimpleAcceptanceAlert(presentingViewController: self)
        imagePicker = ImagePicker(presentingViewController: self)
        imagePicker?.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        arrangeView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        refreshTableViewCellModel()
        refreshNoItemsLabelVisibility()
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewModel.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RecognizedTextTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let model = tableViewModel[indexPath.row]
        cell.set(data: model)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = recognizedTextsManager.getRecognizedTexts()[indexPath.row]
        imageFilesHandler.readImage(id: item.imageId, isThumbnail: false) {
            [weak self] image in
            guard let strongSelf = self, let image = image else {
                log(kind: .warning, message: "RecognizedTextsViewController:tableView:didSelectRow:failed - no data to show")
                return
            }

            strongSelf.delegate?.recognizedTextsViewController(strongSelf, didRequestShowingImage: image, recognizedText: item.recognizedText)
        }
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let title = "RECOGNIZED_TEXTS_SCREEN_DELETE_ALERT_TITLE".localized
            let message = "RECOGNIZED_TEXTS_SCREEN_DELETE_ALERT_MESSAGE".localized
            alert?.show(title: title, message: message) {
                [weak self] answer in
                if answer == .yes {
                    self?.removeRecordedText(atIndex: indexPath.row)
                }
            }
        }
    }
}

extension RecognizedTextsViewController: ImagePickerDelegate {

    func imagePicker(_ picker: ImagePicker, didFinishWithImage image: UIImage?) {
        if let image = image {
            delegate?.recognizedTextsViewController(self, didAddNewImage: image)
        } else {
            log(kind: .warning, message: "RecognizedTextsViewController:imagePicker:didFinishWithImage - empty image returned")
        }
    }
}

private extension RecognizedTextsViewController {

    func removeRecordedText(atIndex index: Int) {
        var recognizedTexts = recognizedTextsManager.getRecognizedTexts()
        let text = recognizedTexts[index]
        recognizedTextsManager.remove(recognizedText: text)
        refreshTableViewCellModel()
        refreshNoItemsLabelVisibility()
        tableView.reloadData()
        imageFilesHandler.deleteImage(withId: text.imageId, isThumbnail: false, callback: nil)
        imageFilesHandler.deleteImage(withId: text.thumbnailId, isThumbnail: true, callback: nil)
    }

    func refreshTableViewCellModel() {
        let recognizedTexts = recognizedTextsManager.getRecognizedTexts()
        var newModel = [RecognizedTextTableViewCellModel]()
        for item in recognizedTexts {
            if let image = imageFilesHandler.readImage(id: item.thumbnailId, isThumbnail: true) {
                let cellModel = RecognizedTextTableViewCellModel(thumbnail: image, text: item.recognizedText.ellipsis(length: AppConstants.ellipsisLength))
                newModel.append(cellModel)
            } else {
                log(kind: .warning, message: "RecognizedTextsViewController:refreshTableViewCellModel - thumbnail image for recognized text not found \(recognizedTexts)")
                let cellModel = RecognizedTextTableViewCellModel(thumbnail: UIImage(), text: item.recognizedText.ellipsis(length: AppConstants.ellipsisLength))
                newModel.append(cellModel)
            }
        }
        tableViewModel = newModel
    }

    func arrangeView() {
        title = "RECOGNIZED_TEXTS_SCREEN_TITLE".localized

        arrangeTableView()
        arrangeNoItemsView()

        navigationItem.rightBarButtonItem = UIBarButtonItem(
                title: "RECOGNIZED_TEXTS_SCREEN_ADD".localized,
                style: .plain,
                target: self,
                action: #selector(RecognizedTextsViewController.didTapAddButton))
    }

    func arrangeTableView() {
        tableView.register(RecognizedTextTableViewCell.self, forCellReuseIdentifier: RecognizedTextTableViewCell.defaultReuseIdentifier)
        tableView.rowHeight = AppConstants.largeTableViewRowHeight
        tableView.backgroundColor = .ocrWarmGrey
        tableView.separatorColor = .darkGray
        tableView.tableFooterView = UIView()    // hide empty rows
    }

    func arrangeNoItemsView() {
        view.addSubview(noItemsLabel)
        noItemsLabel.snp.makeConstraints {
            make in
            make.centerY.equalTo(view.safeAreaLayoutGuide.snp.centerY)
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leadingMargin).inset(AppConstants.genericHeaderSpacing)
            make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailingMargin).inset(AppConstants.genericHeaderSpacing)
        }
    }

    func refreshNoItemsLabelVisibility() {
        noItemsLabel.isHidden = !tableViewModel.isEmpty
    }

    @objc func didTapAddButton() {
        imagePicker?.showImagePicker()
    }
}
