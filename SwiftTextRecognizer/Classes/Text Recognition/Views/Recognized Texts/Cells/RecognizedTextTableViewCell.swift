import Foundation
import UIKit

struct RecognizedTextTableViewCellModel {
    let thumbnail: UIImage
    let text: String
}

class RecognizedTextTableViewCell: UITableViewCell {

    override init(style: CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    func set(data: RecognizedTextTableViewCellModel) {
        textLabel?.text = data.text
        accessoryView = UIImageView.makeGenericImageView(image: data.thumbnail)
    }
}

extension RecognizedTextTableViewCell: ReusableView {
}

private extension RecognizedTextTableViewCell {

    func setupView() {
        textLabel?.font = .ocrCellTextFont
        textLabel?.textColor = .ocrDarkGrey
        textLabel?.numberOfLines = 0
    }
}
