import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class RecognizedTextsViewControllerSpec: QuickSpec {
    override func spec() {
        describe("RecognizedTextsViewController") {

            var fakeRecognizedTextsManager: FakeRecognizedTextsManager!
            var fakeImageFilesHandler: FakeImageFilesHandler!
            var fakeImagePicker: FakeImagePicker!
            var fakeAlert: FakeAcceptanceAlert!
            var fakeDelegate: FakeRecognizedTextsViewControllerDelegate!
            var sut: RecognizedTextsViewController?

            beforeEach {
                fakeRecognizedTextsManager = FakeRecognizedTextsManager()
                fakeImageFilesHandler = FakeImageFilesHandler()
                fakeDelegate = FakeRecognizedTextsViewControllerDelegate()
                fakeAlert = FakeAcceptanceAlert()

                sut = RecognizedTextsViewController(recognizedTextsManager: fakeRecognizedTextsManager, imageFilesHandler: fakeImageFilesHandler)

                sut?.backgroundQueueExecutor = FakeAsynchronousOperationsExecutor()
                sut?.mainQueueExecutor = FakeAsynchronousOperationsExecutor()
                sut?.delegate = fakeDelegate
                sut?.alert = fakeAlert
                fakeImagePicker = FakeImagePicker(presentingViewController: sut!)
                sut?.imagePicker = fakeImagePicker
                fakeImagePicker.delegate = sut
            }

            it("should use provided recognized texts manager") {
                expect(sut?.recognizedTextsManager) === fakeRecognizedTextsManager
            }

            it("should use provided image handler") {
                expect(sut?.imageFilesHandler) === fakeImageFilesHandler
            }

            describe("table view") {
                let fixtureImage = UIImage()
                let fixtureText = "fixtureText"
                let fixtureThumbnailId = "fixtureThId"
                let fixtureImageId = "fixtureImgId"
                let fixtureRecognizedText = RecognizedTextModel(thumbnailId: fixtureThumbnailId, imageId: fixtureImageId, recognizedText: fixtureText)
                let fixtureIndexPath = IndexPath(row: 0, section: 0)
                var cell: RecognizedTextTableViewCell?

                beforeEach {
                    fakeImageFilesHandler.simulatedReadImage = fixtureImage
                    fakeRecognizedTextsManager.simulatedRecognizedTexts = [fixtureRecognizedText]
                    sut?.loadViewIfNeeded()
                    sut?.viewWillAppear(false)
                    cell = sut?.tableView.cellForRow(at: fixtureIndexPath) as? RecognizedTextTableViewCell
                }

                it("should setup table view") {
                    expect(sut?.tableView.numberOfRows(inSection: 0)) == 1
                }

                it("should set proper cell text") {
                    expect(cell?.textLabel?.text) == fixtureRecognizedText.recognizedText
                }

                it("should set proper cell thumbnail") {
                    let imageView = cell?.accessoryView as? UIImageView
                    expect(imageView?.image) == fixtureImage
                }

                it("should NOT show no items label") {
                    expect(sut?.noItemsLabel.isHidden) == true
                }

                describe("adding item") {
                    beforeEach {
                        sut?.navigationItem.rightBarButtonItem?.simulateTap()
                    }

                    it("should open image picker") {
                        fakeImagePicker.verifyCall(withIdentifier: "showImagePicker")
                    }

                    describe("image selected") {
                        beforeEach {
                            fakeImagePicker.simulateSuccess(image: fixtureImage)
                        }

                        it("should notify delegate") {
                            fakeDelegate.verifyCall(withIdentifier: "didAddNewImage", arguments: [fixtureImage.size])
                        }
                    }
                }

                describe("selecting item") {
                    beforeEach {
                        sut?.tableView(sut!.tableView, didSelectRowAt: fixtureIndexPath)
                        fakeImageFilesHandler.readCallback?(fixtureImage)
                    }

                    it("should notify delegate") {
                        fakeDelegate.verifyCall(withIdentifier: "didRequestShowingImage", arguments: [fixtureImage.size, fixtureText])
                    }
                }

                describe("removing item") {
                    beforeEach {
                        sut?.tableView(sut!.tableView, commit: .delete, forRowAt: fixtureIndexPath)
                    }

                    it("should show confirmation alert") {
                        let title = "RECOGNIZED_TEXTS_SCREEN_DELETE_ALERT_TITLE".localized
                        let message = "RECOGNIZED_TEXTS_SCREEN_DELETE_ALERT_MESSAGE".localized
                        fakeAlert.verifyCall(withIdentifier: "show", arguments: [title, message])
                    }

                    describe("accepted") {
                        beforeEach {
                            fakeAlert.simulate(answer: .yes)
                        }

                        it("should remove item") {
                            fakeRecognizedTextsManager.verifyCall(withIdentifier: "remove", arguments: [fixtureRecognizedText])
                        }

                        it("should remove thumbnail") {
                            fakeImageFilesHandler.verifyCall(withIdentifier: "deleteImageCallback", arguments: [fixtureThumbnailId, true])
                        }

                        it("should remove image") {
                            fakeImageFilesHandler.verifyCall(withIdentifier: "deleteImageCallback", arguments: [fixtureImageId, false])
                        }
                    }
                }

                //  todo: add snapshot tests


            }
        }
    }
}

class FakeRecognizedTextsViewControllerDelegate: RecognizedTextsViewControllerDelegate, Mock {
    var storage: [RecordedCall] = []

    func recognizedTextsViewController(_ controller: RecognizedTextsViewController, didAddNewImage image: UIImage) {
        recordCall(withIdentifier: "didAddNewImage", arguments: [image.size])
    }

    func recognizedTextsViewController(_ controller: RecognizedTextsViewController, didRequestShowingImage image: UIImage, recognizedText: String) {
        recordCall(withIdentifier: "didRequestShowingImage", arguments: [image.size, recognizedText])
    }
}

extension CGSize: MockEquatable {
    public func equalTo(other: Any?) -> Bool {
        if let otherSize = other as? CGSize {
            return otherSize == self
        }
        return false
    }
}
