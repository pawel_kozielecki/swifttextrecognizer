import Foundation
import UIKit
import SnapKit

protocol TextRecognitionResultViewDelegate: class {
    func textRecognitionResultViewDidSave(_ view: TextRecognitionResultView)
    func textRecognitionResultViewDidCancel(_ view: TextRecognitionResultView)
}

class TextRecognitionResultView: UIView {

    let isPresentingPreview: Bool
    let cancelButton = UIButton.makeGenericButton(title: "TEXT_RECOGNITION_RESULT_VIEW_CANCEL".localized)
    let acceptButton = UIButton.makeGenericButton(title: "TEXT_RECOGNITION_RESULT_VIEW_ACCEPT".localized)
    let backButton = UIButton.makeGenericBackButton()
    let header = UILabel.makeHeaderLabel(title: "TEXT_RECOGNITION_RESULT_VIEW_HEADER".localized)
    let imageView: UIImageView
    let textView: UITextView

    weak var delegate: TextRecognitionResultViewDelegate?

    init(recognizedText text: String, image: UIImage, showBottomButtons: Bool) {
        self.isPresentingPreview = showBottomButtons
        imageView = UIImageView.makeGenericImageView(image: image)
        textView = UITextView.makeGenericTextView(text: text)

        super.init(frame: .zero)

        backgroundColor = .ocrWarmGrey
        arrangeSubviews()
        applyConstraints()

        acceptButton.addTarget(self, action: #selector(TextRecognitionResultView.didTapAcceptButton(_:)), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(TextRecognitionResultView.didTapCancelButton(_:)), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(TextRecognitionResultView.didTapCancelButton(_:)), for: .touchUpInside)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("not implemented")
    }
}

private extension TextRecognitionResultView {

    @objc func didTapAcceptButton(_ sender: UIButton) {
        delegate?.textRecognitionResultViewDidSave(self)
    }

    @objc func didTapCancelButton(_ sender: UIButton) {
        delegate?.textRecognitionResultViewDidCancel(self)
    }

    func arrangeSubviews() {
        addSubview(imageView)
        addSubview(textView)
        if isPresentingPreview {
            addSubview(header)
            addSubview(acceptButton)
            addSubview(cancelButton)
        } else {
            addSubview(backButton)
        }
    }

    func applyConstraints() {
        let verticalMargin = 2 * AppConstants.genericButtonSpacing + AppConstants.genericButtonHeight
        imageView.snp.makeConstraints {
            make in
            make.top.equalTo(safeAreaLayoutGuide.snp.topMargin).inset(isPresentingPreview ? verticalMargin : 4*AppConstants.genericItemsSpacing)
            make.bottom.equalTo(textView.snp.top).offset(-AppConstants.genericItemsSpacing)
            make.leading.equalTo(safeAreaLayoutGuide.snp.leadingMargin).inset(AppConstants.genericItemsSpacing)
            make.trailing.equalTo(safeAreaLayoutGuide.snp.trailingMargin).inset(AppConstants.genericItemsSpacing)
        }
        textView.snp.makeConstraints {
            make in
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottomMargin).inset(isPresentingPreview ? verticalMargin : AppConstants.genericItemsSpacing)
            make.leading.equalTo(safeAreaLayoutGuide.snp.leadingMargin).inset(AppConstants.genericItemsSpacing)
            make.trailing.equalTo(safeAreaLayoutGuide.snp.trailingMargin).inset(AppConstants.genericItemsSpacing)
            make.height.equalTo(isPresentingPreview ? AppConstants.genericTextViewHeight : AppConstants.enhancedTextViewHeight)
        }
        if isPresentingPreview {
            header.snp.makeConstraints {
                make in
                make.top.equalTo(safeAreaLayoutGuide.snp.topMargin).inset(AppConstants.genericHeaderSpacing)
                make.leading.equalTo(safeAreaLayoutGuide.snp.leadingMargin).inset(AppConstants.genericHeaderSpacing)
                make.trailing.equalTo(safeAreaLayoutGuide.snp.trailingMargin).inset(AppConstants.genericHeaderSpacing)
            }
            cancelButton.snp.makeConstraints {
                make in
                make.height.equalTo(AppConstants.genericButtonHeight)
                make.width.equalToSuperview().dividedBy(2).inset(AppConstants.genericButtonSpacing)
                make.bottom.equalTo(safeAreaLayoutGuide.snp.bottomMargin).inset(AppConstants.genericButtonSpacing)
                make.leading.equalTo(safeAreaLayoutGuide.snp.leadingMargin).offset(AppConstants.genericButtonSpacing)
            }
            acceptButton.snp.makeConstraints {
                make in
                make.height.equalTo(AppConstants.genericButtonHeight)
                make.width.equalToSuperview().dividedBy(2).inset(AppConstants.genericButtonSpacing)
                make.bottom.equalTo(safeAreaLayoutGuide.snp.bottomMargin).inset(AppConstants.genericButtonSpacing)
                make.trailing.equalTo(safeAreaLayoutGuide.snp.trailingMargin).inset(AppConstants.genericButtonSpacing)
            }
        } else {
            backButton.snp.makeConstraints {
                make in
                make.size.equalTo(AppConstants.backArrowButtonSize)
                make.top.equalTo(safeAreaLayoutGuide.snp.topMargin)
                make.leading.equalTo(safeAreaLayoutGuide.snp.leadingMargin)
            }
        }
    }
}
