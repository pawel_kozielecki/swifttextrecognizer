import Foundation
import UIKit

protocol TextRecognitionResultViewControllerDelegate: class {
    func textRecognitionResultViewControllerDidFinish(_ viewController: TextRecognitionResultViewController)
}

class TextRecognitionResultViewController: UIViewController {

    let image: UIImage
    let recognizedText: String
    let isPresentingPreview: Bool
    let imageFilesHandler: ImageFilesHandler
    let recognizedTextsManager: RecognizedTextsManager?

    weak var delegate: TextRecognitionResultViewControllerDelegate?
    var thumbnailGenerator: ThumbnailGenerator = DefaultThumbnailGenerator()
    var infoAlert: InfoAlert?
    var progressHud: ProgressHud?

    init(toDisplayPreviewOfImage image: UIImage,
         withRecognizedText text: String,
         imageFilesHandler: ImageFilesHandler,
         recognizedTextsManager: RecognizedTextsManager) {
        self.image = image
        self.recognizedText = text
        self.imageFilesHandler = imageFilesHandler
        self.recognizedTextsManager = recognizedTextsManager
        self.isPresentingPreview = true

        super.init(nibName: nil, bundle: nil)

        infoAlert = SimpleInfoAlert(presentingViewController: self)
        progressHud = DefaultProgressHud(view: view)
    }

    init(toDisplaySavedImage image: UIImage, withRecognizedText text: String, imageFilesHandler: ImageFilesHandler) {
        self.image = image
        self.recognizedText = text
        self.imageFilesHandler = imageFilesHandler
        self.recognizedTextsManager = nil
        self.isPresentingPreview = false

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let view = TextRecognitionResultView(
                recognizedText: recognizedText,
                image: image,
                showBottomButtons: isPresentingPreview)
        view.delegate = self
        self.view = view
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

extension TextRecognitionResultViewController: TextRecognitionResultViewDelegate {

    func textRecognitionResultViewDidSave(_ view: TextRecognitionResultView) {
        let photo = self.image
        let text = self.recognizedText
        progressHud?.show()

        thumbnailGenerator.getThumbnail(forImage: photo, size: AppConstants.thumbnailSize) {
            [weak self] thumbnail in
            if let thumbnail = thumbnail {
                self?.save(recognizedText: text, withImage: photo, andThumbnail: thumbnail)
            } else {
                self?.progressHud?.hide()
            }
        }
    }

    func textRecognitionResultViewDidCancel(_ view: TextRecognitionResultView) {
        delegate?.textRecognitionResultViewControllerDidFinish(self)
    }
}

private extension TextRecognitionResultViewController {

    func save(recognizedText text: String, withImage image: UIImage, andThumbnail thumbnail: UIImage) {

        // This is a prime candidate for refactoring ;)
        // I should've put into an Operation Queue as these operations can be executed in parallel

        imageFilesHandler.saveImage(thumbnail, asThumbnail: true) {
            [weak self] thumbnailId in
            self?.imageFilesHandler.saveImage(image, asThumbnail: false) {
                [weak self] imageId in
                guard let strongSelf = self, let thumbnailId = thumbnailId, let imageId = imageId else {
                    log(kind: .warning, message: "TextRecognitionResultViewController:save:failed - failed to save recognized text")
                    self?.progressHud?.hide()
                    self?.showErrorAlert()
                    return
                }

                let model = RecognizedTextModel(thumbnailId: thumbnailId, imageId: imageId, recognizedText: text)
                strongSelf.save(recognizedText: model)
                strongSelf.progressHud?.hide()
                strongSelf.delegate?.textRecognitionResultViewControllerDidFinish(strongSelf)
            }
        }
    }

    func save(recognizedText model: RecognizedTextModel) {
        var texts = recognizedTextsManager?.getRecognizedTexts() ?? []
        texts.append(model)
        recognizedTextsManager?.store(recognizedTexts: texts)
    }

    func showErrorAlert() {
        let title = "TEXT_RECOGNITION_RESULT_VIEW_IMAGE_GENERATION_ERROR_TITLE".localized
        let message = "TEXT_RECOGNITION_RESULT_VIEW_IMAGE_GENERATION_ERROR_MESSAGE".localized
        infoAlert?.show(title: title, message: message, callback: nil)
    }
}
