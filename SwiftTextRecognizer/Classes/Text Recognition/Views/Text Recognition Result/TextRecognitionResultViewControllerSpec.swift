import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class TextRecognitionResultViewControllerSpec: QuickSpec {
    override func spec() {
        describe("TextRecognitionResultViewController") {

            let fixtureImage = UIImage()
            let fixtureRecognizedText = "fixtureRecognizedText"

            var fakeImageFilesHandler: FakeImageFilesHandler!
            var fakeDelegate: FakeTextRecognitionResultViewControllerDelegate!
            var sut: TextRecognitionResultViewController?

            beforeEach {
                fakeDelegate = FakeTextRecognitionResultViewControllerDelegate()
                fakeImageFilesHandler = FakeImageFilesHandler()
            }

            describe("init in preview mode") {
                var view: TextRecognitionResultView?

                beforeEach {
                    sut = TextRecognitionResultViewController(
                            toDisplaySavedImage: fixtureImage, withRecognizedText: fixtureRecognizedText, imageFilesHandler: fakeImageFilesHandler)
                    sut?.delegate = fakeDelegate
                    sut?.loadViewIfNeeded()
                    view = sut?.view as? TextRecognitionResultView
                }

                it("should use provided image") {
                    expect(sut?.image) === fixtureImage
                }

                it("should use provided recognized text") {
                    expect(sut?.recognizedText) == fixtureRecognizedText
                }

                it("should use provided images handler") {
                    expect(sut?.imageFilesHandler) === fakeImageFilesHandler
                }

                it("should set recognized texts manager as nil") {
                    expect(sut?.recognizedTextsManager).to(beNil())
                }

                it("should create view and become its delegate") {
                    expect(view?.delegate) === sut
                }

                describe("going back") {
                    beforeEach {
                        view?.backButton.simulateTap()
                    }

                    it("should notify delegate") {
                        fakeDelegate.verifyCall(withIdentifier: "didFinish")
                    }
                }

                //  todo: add snapshot tests
            }

            describe("init in acceptance mode") {
                var fakeRecognizedTextsManager: FakeRecognizedTextsManager!
                var fakeThumbnailGenerator: FakeThumbnailGenerator!
                var view: TextRecognitionResultView?

                beforeEach {
                    fakeRecognizedTextsManager = FakeRecognizedTextsManager()
                    fakeThumbnailGenerator = FakeThumbnailGenerator()
                    sut = TextRecognitionResultViewController(
                            toDisplayPreviewOfImage: fixtureImage, withRecognizedText: fixtureRecognizedText,
                            imageFilesHandler: fakeImageFilesHandler, recognizedTextsManager: fakeRecognizedTextsManager)
                    sut?.thumbnailGenerator = fakeThumbnailGenerator
                    sut?.delegate = fakeDelegate
                    sut?.loadViewIfNeeded()
                    view = sut?.view as? TextRecognitionResultView
                }

                it("should use provided image") {
                    expect(sut?.image) === fixtureImage
                }

                it("should use provided recognized text") {
                    expect(sut?.recognizedText) == fixtureRecognizedText
                }

                it("should use provided images handler") {
                    expect(sut?.imageFilesHandler) === fakeImageFilesHandler
                }

                it("should use provided recognized texts manager") {
                    expect(sut?.recognizedTextsManager) === fakeRecognizedTextsManager
                }

                it("should create view and become its delegate") {
                    expect(view?.delegate) === sut
                }

                describe("accepting text recognition") {
                    beforeEach {
                        view?.acceptButton.simulateTap()
                    }

                    it("should generate thumbnail") {
                        fakeThumbnailGenerator.verifyCall(withIdentifier: "getThumbnail", arguments: [fixtureImage.size, CGSize(width: 50, height: 50)])
                    }

                    describe("success") {
                        let fixtureThImage = UIImage()
                        let fixtureImageId = "fixtureImageId"
                        let fixtureThumbnailId = "fixtureThumbnailId"

                        beforeEach {
                            fakeThumbnailGenerator.simulateThumbnailGenerated(image: fixtureThImage)
                            fakeImageFilesHandler.saveThCallback?(fixtureThumbnailId)
                            fakeImageFilesHandler.saveImgCallback?(fixtureImageId)
                        }

                        it("should save image") {
                            fakeImageFilesHandler.verifyCall(withIdentifier: "saveImageCallback", arguments: [fixtureImage.size, false])
                        }

                        it("should save thumbnail") {
                            fakeImageFilesHandler.verifyCall(withIdentifier: "saveImageCallback", arguments: [fixtureThImage.size, true])
                        }

                        it("should save recognized text") {
                            let fixtureModel = RecognizedTextModel(thumbnailId: fixtureThumbnailId, imageId: fixtureImageId, recognizedText: fixtureRecognizedText)
                            fakeRecognizedTextsManager.verifyCall(withIdentifier: "store", arguments: [[fixtureModel]])
                        }

                        it("should notify delegate") {
                            fakeDelegate.verifyCall(withIdentifier: "didFinish")
                        }
                    }
                }

                describe("rejecting text recognition") {
                    beforeEach {
                        view?.cancelButton.simulateTap()
                    }

                    it("should notify delegate") {
                        fakeDelegate.verifyCall(withIdentifier: "didFinish")
                    }
                }

                //  todo: add snapshot tests
            }
        }
    }
}

class FakeTextRecognitionResultViewControllerDelegate: TextRecognitionResultViewControllerDelegate, Mock {
    var storage: [RecordedCall] = []

    func textRecognitionResultViewControllerDidFinish(_ viewController: TextRecognitionResultViewController) {
        recordCall(withIdentifier: "didFinish")
    }
}
