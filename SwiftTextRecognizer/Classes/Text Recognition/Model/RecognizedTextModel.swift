import Foundation

struct RecognizedTextModel {
    let thumbnailId: String
    let imageId: String
    let recognizedText: String
}

extension RecognizedTextModel: Codable, Equatable {
}
