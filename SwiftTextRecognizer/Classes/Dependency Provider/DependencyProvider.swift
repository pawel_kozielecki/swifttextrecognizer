import Foundation

protocol DependencyProvider: class {
    var visibleViewControllerProvider: VisibleViewControllerProvider { get }
    var textRecognizer: TextRecognizer { get }
    var imageFilesHandler: ImageFilesHandler { get }
    var recognizedTextsManager: RecognizedTextsManager { get }

    func setup(windowController: WindowController)
}

class DefaultDependencyProvider {
    var windowController: WindowController!
    var textRecognizer: TextRecognizer
    var imageFilesHandler: ImageFilesHandler
    var recognizedTextsManager: RecognizedTextsManager

    init() {
        textRecognizer = DefaultTextRecognizer()
        imageFilesHandler = DefaultImageFilesHandler()
        recognizedTextsManager = DefaultRecognizedTextsManager()

    }

    func setup(windowController: WindowController) {
        log(message: "DependencyProvider:setup")
        self.windowController = windowController
    }
}

extension DefaultDependencyProvider: DependencyProvider {

    var visibleViewControllerProvider: VisibleViewControllerProvider {
        return windowController
    }
}
