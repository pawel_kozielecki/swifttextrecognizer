import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class DefaultDependencyProviderSpec: QuickSpec {
    override func spec() {
        describe("DefaultDependencyProvider") {

            var sut: DefaultDependencyProvider?

            beforeEach {
                sut = DefaultDependencyProvider()
            }

            describe("post-constructor setup") {
                var fakeWindowController: FakeWindowController!

                beforeEach {
                    fakeWindowController = FakeWindowController()
                    sut?.setup(windowController: fakeWindowController)
                }

                it("should use provided windows controller") {
                    expect(sut?.windowController) === fakeWindowController
                }
            }
        }
    }
}

class FakeWindowController: WindowController, Mock {
    var storage: [RecordedCall] = []

    init() {
        super.init(dependencyProvider: FakeDependencyProvider())
    }

    override func makeAndPresentInitialViewController() {
        recordCall(withIdentifier: "makeAndPresentInitialViewController")
    }
}
