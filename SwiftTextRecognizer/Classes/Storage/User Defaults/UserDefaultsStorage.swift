import Foundation

class UserDefaultsStorage: LocalStorage {

    var defaults: UserDefaults = UserDefaults.standard

    func readData(key: String) -> Data? {
        return defaults.data(forKey: key)
    }

    func write(key: String, value: Data) {
        defaults.set(value, forKey: key)
    }

    func invalidate(key: String) {
        defaults.removeObject(forKey: key)
    }
}
