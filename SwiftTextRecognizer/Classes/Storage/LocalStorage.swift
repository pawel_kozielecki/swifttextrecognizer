import Foundation

protocol LocalStorage {

    func readData(key: String) -> Data?
    func write(key: String, value: Data)
    func invalidate(key: String)
}

extension LocalStorage {

    func invalidate(key: String) {
    }
}
