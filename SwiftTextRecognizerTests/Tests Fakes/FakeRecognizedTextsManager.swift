import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class FakeRecognizedTextsManager: RecognizedTextsManager, Mock {

    var storage: [RecordedCall] = []
    var simulatedRecognizedTexts: [RecognizedTextModel]?

    func getRecognizedTexts() -> [RecognizedTextModel] {
        return simulatedRecognizedTexts ?? []
    }

    func store(recognizedTexts: [RecognizedTextModel]) {
        recordCall(withIdentifier: "store", arguments: [recognizedTexts])
    }

    func remove(recognizedText: RecognizedTextModel) {
        recordCall(withIdentifier: "remove", arguments: [recognizedText])
    }

    func invalidateRecognizedTexts() {
        recordCall(withIdentifier: "invalidate")
    }
}

extension RecognizedTextModel: MockEquatable {

    public func equalTo(other: Any?) -> Bool {
        if let otherModel = other as? RecognizedTextModel {
            return self == otherModel
        }
        return false
    }
}
