import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class FakeThumbnailGenerator: ThumbnailGenerator, Mock {

    var storage: [RecordedCall] = []
    var callback: ((UIImage?) -> Void)?

    func getThumbnail(forImage image: UIImage, size: CGSize, callback: ((UIImage?) -> Void)?) {
        self.callback = callback
        recordCall(withIdentifier: "getThumbnail", arguments: [image.size, size])
    }

    func simulateThumbnailGenerated(image: UIImage?) {
        callback?(image)
    }
}
