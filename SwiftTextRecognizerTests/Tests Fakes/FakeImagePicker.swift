import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class FakeImagePicker: ImagePicker, Mock {
    var storage: [RecordedCall] = []

    override init(presentingViewController: UIViewController) {
        super.init(presentingViewController: presentingViewController)
        backgroundQueueExecutor = FakeAsynchronousOperationsExecutor()
        mainQueueExecutor = FakeAsynchronousOperationsExecutor()
    }

    override func showImagePicker() {
        recordCall(withIdentifier: "showImagePicker")
    }

    func simulateSuccess(image: UIImage?) {
        delegate?.imagePicker(self, didFinishWithImage: image)
    }

    func simulateCancel() {
        delegate?.imagePickerDidCancel(self)
    }
}
