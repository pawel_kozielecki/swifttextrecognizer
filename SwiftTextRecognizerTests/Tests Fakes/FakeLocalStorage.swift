import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class FakeLocalStorage: LocalStorage, Mock {

    var storage: [RecordedCall] = []
    var simulatedValue: Any?

    func readData(key: String) -> Data? {
        return simulatedValue as? Data
    }

    func write(key: String, value: Data) {
        recordCall(withIdentifier: "write", arguments: [key, value])
    }

    func invalidate(key: String) {
        recordCall(withIdentifier: "invalidate", arguments: [key])
    }
}
