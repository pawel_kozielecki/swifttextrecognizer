import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class FakeUINavigationController: UINavigationController, Mock {

    private (set) var lastPushedViewController: UIViewController?

    var storage: [RecordedCall] = []

    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        lastPushedViewController = viewController
        viewControllers.append(viewController)
    }

    override func popViewController(animated: Bool) -> UIViewController? {
        recordCall(withIdentifier: "popViewController", arguments: [animated])
        let vc = viewControllers.popLast()
        lastPushedViewController = viewControllers.last
        return vc
    }
    override func popToRootViewController(animated: Bool) -> [UIViewController]? {
        recordCall(withIdentifier: "popToRootViewController", arguments: [animated])
        lastPushedViewController = viewControllers.first
        return lastPushedViewController != nil ? [lastPushedViewController!] : nil
    }
}

extension UIViewController {

    var className: String {
        return NSStringFromClass(type(of: self))
    }
}
