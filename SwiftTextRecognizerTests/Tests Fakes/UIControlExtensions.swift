import Foundation
import UIKit

extension UIControl {

    func simulateTap() {
        sendActions(for: .touchUpInside)
    }
}

extension UIBarButtonItem {

    func simulateTap() {
        if let target = self.target {
            _ = target.perform(action, with: self)
        }
    }
}
