import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class FakeDependencyProvider: DependencyProvider, Mock {
    var storage: [RecordedCall] = []
    var windowController: WindowController!
    var fakeTextRecognizer = FakeTextRecognizer()
    var fakeImageFilesHandler = FakeImageFilesHandler()
    var fakeRecognizedTextsManager = FakeRecognizedTextsManager()

    var visibleViewControllerProvider: VisibleViewControllerProvider {
        return windowController!
    }

    var textRecognizer: TextRecognizer {
        return fakeTextRecognizer
    }

    var imageFilesHandler: ImageFilesHandler {
        return fakeImageFilesHandler
    }

    var recognizedTextsManager: RecognizedTextsManager {
        return fakeRecognizedTextsManager
    }

    func setup(windowController: WindowController) {
        self.windowController = windowController
        recordCall(withIdentifier: "setup")
    }
}
