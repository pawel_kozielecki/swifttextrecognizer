import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class FakeImageFilesHandler: ImageFilesHandler, Mock {

    var storage: [RecordedCall] = []
    var simulatedReadImage: UIImage?
    var simulatedSavedImageId: String?
    var readCallback: ((UIImage?) -> Void)?
    var saveThCallback: ((String?) -> Void)?
    var saveImgCallback: ((String?) -> Void)?
    var removeCallback: ((Bool) -> Void)?

    func readImage(id: String, isThumbnail: Bool) -> UIImage? {
        recordCall(withIdentifier: "readImage", arguments: [id, isThumbnail])
        return simulatedReadImage
    }

    func saveImage(_ image: UIImage, asThumbnail: Bool) -> String? {
        recordCall(withIdentifier: "saveImage", arguments: [image.size, asThumbnail])
        return simulatedSavedImageId
    }

    func deleteImage(withId id: String, isThumbnail: Bool) -> Bool {
        recordCall(withIdentifier: "deleteImage", arguments: [id, isThumbnail])
        return true
    }

    func readImage(id: String, isThumbnail: Bool, callback: ((UIImage?) -> Void)?) {
        recordCall(withIdentifier: "readImageCallback", arguments: [id, isThumbnail])
        self.readCallback = callback
    }

    func saveImage(_ image: UIImage, asThumbnail: Bool, callback: ((String?) -> Void)?) {
        recordCall(withIdentifier: "saveImageCallback", arguments: [image.size, asThumbnail])
        if asThumbnail {
            self.saveThCallback = callback
        } else {
            self.saveImgCallback = callback
        }
    }

    func deleteImage(withId id: String, isThumbnail: Bool, callback: ((Bool) -> Void)?) {
        recordCall(withIdentifier: "deleteImageCallback", arguments: [id, isThumbnail])
        self.removeCallback = callback
    }
}
