import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class FakeInfoAlert: InfoAlert, Mock {
    var storage: [RecordedCall] = []
    var callback: (() -> Void)?

    func show(title: String?, message: String?, callback: (() -> Void)?) {
        self.callback = callback
        recordCall(withIdentifier: "show", arguments: [title, message])
    }

    func showOnTop(title: String?, message: String?, callback: (() -> Void)?) {
        self.callback = callback
        recordCall(withIdentifier: "showOnTop", arguments: [title, message])
    }

    func dismiss(animated: Bool) {
        recordCall(withIdentifier: "dismiss")
    }
}
