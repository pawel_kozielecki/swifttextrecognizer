import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class FakeTextRecognizer: TextRecognizer, Mock {
    var callback: ((String?) -> Void)?
    var storage: [RecordedCall] = []

    func recognizeText(forImage image: UIImage, callback: ((String?) -> Void)?) {
        self.callback = callback
        recordCall(withIdentifier: "recognizeText", arguments: [image.size])
    }

    func simulateSuccess(text: String) {
        callback?(text)
    }

    func simulateFailure() {
        callback?(nil)
    }
}
