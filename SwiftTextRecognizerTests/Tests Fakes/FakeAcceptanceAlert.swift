import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class FakeAcceptanceAlert: AcceptanceAlert, Mock {
    var storage: [RecordedCall] = []
    var additionalData: Any? = nil
    var completion: ((AcceptanceAlertAnswer) -> Void)?

    func show(title: String?, message: String?, completion: ((AcceptanceAlertAnswer) -> Void)?) {
        self.completion = completion
        recordCall(withIdentifier: "show", arguments: [title, message])
    }

    func dismiss(animated: Bool) {
        recordCall(withIdentifier: "dismiss")
    }

    func simulate(answer: AcceptanceAlertAnswer) {
        completion?(answer)
    }
}
