import Foundation
import Quick
import Nimble
import Mimus

@testable import Swift_OCR

class FakeAsynchronousOperationsExecutor: AsynchronousOperationsExecutor {

    private(set) var queue: OperationQueue? = nil
    private(set) var type: AsynchronousExecutorType = .main

    func execute(_ block: @escaping () -> Void) {
        block()
    }
}
